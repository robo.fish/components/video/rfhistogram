# RFHistogram

Provides a histogram view, rendered with Metal.

Based on code from my project [ManualCamera](https://github.com/robo-fish/ManualCamera) .
