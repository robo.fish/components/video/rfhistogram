// swift-tools-version:5.3

import PackageDescription

let package = Package(
	name: "RFHistogram",
	platforms: [.iOS(.v13)],
	products: [
		.library(
			name: "RFHistogram",
			targets: ["RFHistogram"]),
	],
	dependencies: [
		.package(name: "RFVideoStream", url: "https://gitlab.com/robo.fish/components/video/rfvideostream", from: "0.2.0"),
	],
	targets: [
		.target(
			name: "RFHistogram",
			dependencies: ["RFVideoStream"],
			resources: [.process("Resources/histogram.metal")]
		),
	]
)
