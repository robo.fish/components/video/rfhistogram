//  Copyright 2020 Robo.Fish UG

import CoreVideo
import AVFoundation
import SwiftUI
import UIKit
import Metal
import RFVideoStream

public struct RFHistogramView : UIViewRepresentable
{
	private let _sink : Sink

	public init(stream : RFVideoStream)
	{
		//_view = MTKView(frame: .zero, device: MTLCreateSystemDefaultDevice())
		//_view.isPaused = true
		//_view.enableSetNeedsDisplay = true
		_sink = Sink(stream : stream)
	}

	public func makeUIView(context: Context) -> UIView
	{
		return _sink.view!
	}

	public func updateUIView(_ uiView: UIView, context: Context)
	{

	}

	class Sink : RFVideoSink
	{
		private var _view = MetalView(frame:.zero)
		private var _histogram : RFHistogram
		weak var _stream : RFVideoStream?

		init(stream : RFVideoStream)
		{
			_histogram = RFHistogram(renderLayer: _view.layer as! CAMetalLayer)
			_stream = stream
			_stream?.sink = self
		}

		public var videoSession: AVCaptureSession?
		{
			get { nil }
			set { }
		}

		public var view: UIView?
		{
			_view.isHidden = false
			_view.layer.isHidden = false
			return _view
		}

		public func process(_ buffer : CVPixelBuffer) -> Bool
		{
			DispatchQueue.main.async {
				self._histogram.updateFromPixelData(buffer, completionHandler: {  })
			}
			return true
		}
	}
}

fileprivate class MetalView : UIView
{
	public override class var layerClass: AnyClass
	{
		CAMetalLayer.self
	}
}
